export interface Member {
  tag: string;
  id: string;
  name: string;
  desc: string;
  avatar: string;
}
