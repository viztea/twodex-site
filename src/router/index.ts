import Vue from "vue";
import VueRouter, {
  RouteConfig,
} from "vue-router";
import Home from "../views/Home.vue";
import Team from "@/components/Team.vue";
import Construction from "@/views/Construction.vue";
import {
  join,
} from "path";

Vue.use(VueRouter);
const production = process.env.NODE_ENV === "production";
const get = (path: string) => import(join("@/components", path));

const routes: RouteConfig[] = [{
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/team",
    name: "team",
    component: Construction, // production ? Construction : Team,
  },
  {
    path: "*",
    component: () => import("@/views/404.vue"),
  },
  {
    path: "/projects",
    component: () => import("@/components/projects/index.vue"),
  },
  {
    path: "/projects/hryra",
    component: () => import("@/components/projects/hryra.vue"),
  },
  {
    path: "/projects/betalogger",
    component: () => import("@/components/projects/betalogger.vue"),
  },
  {
    path: "/projects/defymusic",
    component: () => import("@/components/projects/defymusic.vue"),
  },
  {
    path: "/projects/cordclient",
    component: () => import("@/components/projects/cordclient.vue"),
  },
  {
    path: "/link",
    redirect: (e): string => {
      switch (e.query.to) {
        case "discord":
          window.location.href = "https://discord.gg/FHR2msy";
          break;
        case "github":
          window.location.href = "https://github.com/twodex";
          break;
      }
      return "/";
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
